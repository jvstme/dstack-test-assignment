# dstack test assignment

## Requirements

- The program should create a Docker container using the given Docker image name, and
the given bash command.
- The program should handle the output logs of the container and send them to the given
AWS CloudWatch group/stream using the given AWS credentials. If the corresponding
AWS CloudWatch group or stream does not exist, it should create it using the given
AWS credentials.
- The program should behave properly regardless of how much or what kind of logs the
container outputs.
- The program should gracefully handle errors and interruptions.

## Installation

```shell
poetry install --no-root
poetry shell
```

## Usage

Example:

```shell
python main.py --docker-image python --bash-command $'pip install pip -U && pip install tqdm && python -uc \"import time\ncounter = 0\nwhile True:\n\tprint(counter)\n\tcounter = counter + 1\n\ttime.sleep(0.1)\"' --aws-cloudwatch-group test-task-group-1 --aws-cloudwatch-stream test-task-stream-1 --aws-access-key-id ... --aws-secret-access-key ... --aws-region ...
```

See the full list of options:

```shell
python main.py --help
```

## Development

Run code quality tools:

```shell
ruff format
ruff .
mypy .
```
