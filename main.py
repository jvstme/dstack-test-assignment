import logging
import time
from dataclasses import asdict, dataclass
from functools import cached_property
from threading import Lock, Thread
from typing import Annotated, Any, Iterable

import boto3  # type: ignore
import botocore.exceptions  # type: ignore
import dateutil.parser
import docker  # type: ignore
import tenacity
import typer
from docker.models.containers import Container  # type: ignore

CLOUDWATCH_MAX_PUBLISH_INTERVAL_SECS = 5
CLOUDWATCH_MIN_WAIT_BEFORE_RETRY = 1
CLOUDWATCH_MAX_WAIT_BEFORE_RETRY = 8

# AWS constraints
CLOUDWATCH_MAX_BATCH_LEN = 10_000
CLOUDWATCH_MAX_BATCH_SIZE_BYTES = 1_048_576
CLOUDWATCH_EXTRA_BYTES_PER_EVENT = 26
CLOUDWATCH_MAX_MESSAGE_LEN = 64_000  # Up to 256 KB


logging.basicConfig(
    format="[%(asctime)s %(levelname)s] %(message)s", datefmt="%Y-%m-%d %H:%M:%S"
)
logger = logging.getLogger(__name__)


@dataclass(frozen=True)
class LogEvent:
    timestamp: int
    message: str

    @cached_property
    def size_bytes(self) -> int:
        return len(self.message.encode("utf-8")) + CLOUDWATCH_EXTRA_BYTES_PER_EVENT


class LogEventsBatch:
    def __init__(self) -> None:
        self._events: list[LogEvent] = []
        self._size_bytes = 0

    def try_push(self, event: LogEvent) -> bool:
        if (
            len(self._events) == CLOUDWATCH_MAX_BATCH_LEN
            or self._size_bytes + event.size_bytes > CLOUDWATCH_MAX_BATCH_SIZE_BYTES
        ):
            return False

        self._events.append(event)
        self._size_bytes += event.size_bytes
        return True

    def serialize(self) -> list[dict[str, Any]]:
        return [asdict(event) for event in self._events]

    def __len__(self) -> int:
        return len(self._events)


class CloudWatchPublisher:
    def __init__(
        self,
        aws_access_key_id: str,
        aws_secret_access_key: str,
        aws_region: str,
        cloudwatch_group: str,
        cloudwatch_stream: str,
    ) -> None:
        self._client = boto3.client(
            "logs",
            aws_access_key_id=aws_access_key_id,
            aws_secret_access_key=aws_secret_access_key,
            region_name=aws_region,
        )
        self._group = cloudwatch_group
        self._stream = cloudwatch_stream
        self._events_batch = LogEventsBatch()
        self._lock = Lock()
        self._last_publish_ts = 0.0

    def ensure_remote_resources_exist(self) -> None:
        already_exists = self._client.exceptions.ResourceAlreadyExistsException

        try:
            self._client.create_log_group(logGroupName=self._group)
            logger.info(f"Created CloudWatch group {self._group}")
        except already_exists:
            pass

        try:
            self._client.create_log_stream(
                logGroupName=self._group, logStreamName=self._stream
            )
            logger.info(f"Created CloudWatch stream {self._stream}")
        except already_exists:
            pass

    def register_log_event(self, event: LogEvent) -> None:
        with self._lock:
            could_push = self._events_batch.try_push(event)

            if not could_push:
                self.publish()
                self._events_batch.try_push(event)

    def publish_periodically(self) -> None:
        while True:
            with self._lock:
                secs_since_last_publish = time.time() - self._last_publish_ts

                if secs_since_last_publish >= CLOUDWATCH_MAX_PUBLISH_INTERVAL_SECS:
                    self.publish()
                    secs_since_last_publish = 0

            time.sleep(CLOUDWATCH_MAX_PUBLISH_INTERVAL_SECS - secs_since_last_publish)

    @tenacity.retry(
        retry=tenacity.retry_if_exception_type(
            (
                botocore.exceptions.ConnectionError,
                botocore.exceptions.HTTPClientError,
            )
        ),
        wait=tenacity.wait_exponential(
            min=CLOUDWATCH_MIN_WAIT_BEFORE_RETRY,
            max=CLOUDWATCH_MAX_WAIT_BEFORE_RETRY,
        ),
        before_sleep=tenacity.before_sleep_log(logger, logging.ERROR),
    )
    def publish(self) -> None:
        if not self._events_batch:
            return

        logger.info(f"Publishing {len(self._events_batch)} log events to CloudWatch")

        self._client.put_log_events(
            logGroupName=self._group,
            logStreamName=self._stream,
            logEvents=self._events_batch.serialize(),
        )

        self._events_batch = LogEventsBatch()
        self._last_publish_ts = time.time()


def into_chunks(message: str, chars_per_chunk: int) -> list[str]:
    return [
        message[i : i + chars_per_chunk]
        for i in range(0, len(message), chars_per_chunk)
    ]


def convert_docker_log_entry_to_cloudwatch_events(entry: bytes) -> list[LogEvent]:
    line = entry.decode("utf-8", "replace").rstrip("\n")

    datetime_str, message = line.split(" ", 1)
    timestamp = int(dateutil.parser.parse(datetime_str).timestamp() * 1000)
    message_chunks = into_chunks(message, CLOUDWATCH_MAX_MESSAGE_LEN)

    return [LogEvent(message=chunk, timestamp=timestamp) for chunk in message_chunks]


def process_logs_stream(logs: Iterable[bytes], publisher: CloudWatchPublisher) -> None:
    for entry in logs:
        for event in convert_docker_log_entry_to_cloudwatch_events(entry):
            publisher.register_log_event(event)
            logger.debug(f"-> {event.message}")


def main(
    docker_image: Annotated[str, typer.Option()],
    bash_command: Annotated[str, typer.Option()],
    aws_cloudwatch_group: Annotated[str, typer.Option()],
    aws_cloudwatch_stream: Annotated[str, typer.Option()],
    aws_access_key_id: Annotated[str, typer.Option()],
    aws_secret_access_key: Annotated[str, typer.Option()],
    aws_region: Annotated[str, typer.Option()],
    verbose: Annotated[bool, typer.Option()] = False,
) -> None:
    """
    Run a docker container and publish its logs to AWS CloudWatch.

    Log events are batched and each batch is published once in several seconds or when
    the batch size reaches AWS limits.
    """

    logger.setLevel(logging.INFO if not verbose else logging.DEBUG)

    logger.info("Setting up CloudWatch")

    cloudwatch_publisher = CloudWatchPublisher(
        aws_access_key_id,
        aws_secret_access_key,
        aws_region,
        aws_cloudwatch_group,
        aws_cloudwatch_stream,
    )
    cloudwatch_publisher.ensure_remote_resources_exist()
    Thread(target=cloudwatch_publisher.publish_periodically, daemon=True).start()

    logger.info("Setting up Docker")

    docker_client = docker.from_env()
    container_command = f"bash -c '{bash_command}'"
    container: Container = docker_client.containers.run(
        image=docker_image,
        command=container_command,
        detach=True,
    )
    logs = container.logs(stream=True, timestamps=True)

    logger.info("The container is running")

    try:
        process_logs_stream(logs, cloudwatch_publisher)

    except KeyboardInterrupt:
        logger.info("Interrupted")

    else:
        logger.info("The container has exited")

    finally:
        logger.info("Initiating graceful shutdown")
        container.stop()
        process_logs_stream(logs, cloudwatch_publisher)
        cloudwatch_publisher.publish()
        container.remove()


if __name__ == "__main__":
    typer.run(main)
